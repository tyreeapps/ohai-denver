//
//  PointOfInterestIngestOperation.swift
//  ohaidenver
//
//  Created by Walter Tyree.
//  Copyright © 2016 Tyree Apps, LLC. All rights reserved.
//

//  Borrowed respectfully from
//  Created by Marcus Zarra on 5/5/16.
//  Copyright © 2016 Marcus Zarra. All rights reserved.
//
// I mean, seriously, this guy wrote the book on this stuff.
// https://pragprog.com/book/mzswift/core-data-in-swift
// http://martiancraft.com/blog/2015/03/core-data-stack/
// https://realm.io/news/marcus-zarra-core-data-threading/
//
//

import UIKit

class IngestOperation: NSOperation {
    var localURL: NSURL? = nil
    var dataController: DataController?
    
    //Parsing Keys
    let kItemTag = "Placemark"
    let kDescriptionTag = "description"
    let kLocationTag = "coordinates"
    let kNameTag = "name"
    let kIdTag = "id"
    let kCategoryTag = "category"
    
    var currentPOI =  Dictionary<String,AnyObject>()
    var categoryValue: String = ""
    
    var xmlDOMChecker = Array<String>()
    var chunksOfStrings: String = ""
    
    override init() {
        super.init()
        
        let cachesFolder = try! NSFileManager.defaultManager().URLForDirectory(.CachesDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
        self.localURL = cachesFolder.URLByAppendingPathComponent("some_random_file")
        
        guard let delegate = UIApplication.sharedApplication().delegate as? AppDelegate else {return}
        dataController = delegate.dataController
        
        
    }
    
    var finishedValue: Bool = false
    override var finished: Bool {
        set {
            willChangeValueForKey("isFinished")
            finishedValue = newValue
            didChangeValueForKey("isFinished")
        }
        get {
            return finishedValue
        }
    }
    
    override func start() {
        if cancelled {
            finished = true
            return
        }
        
        guard let url = self.localURL else {
            finished = true
            return
        }
        
        guard let parser = NSXMLParser(contentsOfURL: url) else {
            finished = true
            return
        }
        
        parser.delegate = self
        parser.parse()
    }
    
    
    func processData() {

        let existingPlace = dataController?.thePlaces.filter({ (place) -> Bool in
            if let id = self.currentPOI[self.kIdTag] as? String, let category = self.currentPOI[self.kCategoryTag] as? String {
            if place.id == id && place.category == category {
                return true
                } else {
                return false
            }
            } else {
                return false
            }
        })
        
        if let place = existingPlace?.first {
            place.updateWithDictionary(self.currentPOI)
        } else {
            let newPlace = Place()
            newPlace.populateFromDictionary(self.currentPOI)
            dataController?.thePlaces.append(newPlace)
        }
    }
    
}

extension IngestOperation: NSXMLParserDelegate {
    
    func parserDidEndDocument(parser: NSXMLParser) {
        dispatch_async(dispatch_get_main_queue()) { 
            NSNotificationCenter.defaultCenter().postNotificationName("Ingestion Done", object: nil)
        }
    }
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        
        xmlDOMChecker.append(elementName)
        chunksOfStrings = ""
        
        switch elementName {
        case "Document":
            if let category = attributeDict["id"] {
                self.categoryValue = category
            }
        case kItemTag: //starting a new element
            self.currentPOI.removeAll()
            if let idTag = attributeDict["id"] {
                self.currentPOI[kIdTag] = idTag
            }
            self.currentPOI[kCategoryTag] = self.categoryValue
        default:
            break
        }
    }
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        guard elementName == xmlDOMChecker.last else {
            parser.abortParsing()
            finished = true
            return
        }
        
        xmlDOMChecker.removeLast()
        
        switch elementName {
        case kItemTag:
            processData()
        case kNameTag:
            self.currentPOI[kNameTag] = chunksOfStrings
        case kLocationTag:
            self.currentPOI[kLocationTag] = chunksOfStrings
        default:
            break
        }
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        self.chunksOfStrings.appendContentsOf(string)
    }
    
    func parser(parser: NSXMLParser, foundCDATA CDATABlock: NSData) {
        if let descriptionString =  String(data: CDATABlock, encoding: NSUTF8StringEncoding) {
            self.currentPOI[kDescriptionTag] = descriptionString
        }
    }
    
    func parser(parser: NSXMLParser, parseErrorOccurred parseError: NSError) {
        print("Parse error\(parseError)")
        finished = true
        
    }
    
}
