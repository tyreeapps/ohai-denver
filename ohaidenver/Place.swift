//
//  Place+CoreDataClass.swift
//  ohaidenver
//
//  Created by Walter Tyree.
//  Copyright © 2016 Tyree Apps, LLC. All rights reserved.
//

import Foundation

public class Place: NSObject {

    public var name: String?
    public var latitude: NSNumber?
    public var longitude: NSNumber?
    public var detail: String?
    public var id: String?
    public var category: String?
    
    func updateWithDictionary(dict: Dictionary<String,AnyObject>) {
        
        if let name = dict["name"] as? String {
            self.name = name
        }
        
        if let detail = dict["description"] as? String {
            self.detail = detail
        }
        
        if let coordinates = dict["coordinates"] as? String {
            let parsedCoordinates = coordinates.componentsSeparatedByString(",")
            self.latitude = Double(parsedCoordinates[0])
            self.longitude = Double(parsedCoordinates[1])
        }
    }

    
    func populateFromDictionary(dict: Dictionary<String,AnyObject>) {
        
        if let name = dict["name"] as? String {
            self.name = name
        }
        
        if let detail = dict["description"] as? String {
            self.detail = detail
        }
        
        if let cat = dict["category"] as? String {
            self.category = cat
        }
        
        if let id = dict["id"] as? String {
            self.id = id
        }
        
        if let coordinates = dict["coordinates"] as? String {
            let parsedCoordinates = coordinates.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()).componentsSeparatedByString(",")
            self.latitude = Double(parsedCoordinates[0])
            self.longitude = Double(parsedCoordinates[1])
        }
    }
    
}

import MapKit
extension Place: MKAnnotation {
    // Center latitude and longitude of the annotation view.
    // The implementation of this property must be KVO compliant.
    public var coordinate: CLLocationCoordinate2D {
        get {
            if let latitude = self.latitude, let longitude = self.longitude {
                return CLLocationCoordinate2D(latitude: latitude.doubleValue, longitude: longitude.doubleValue)
            }
            return CLLocationCoordinate2DMake(0.0, 0.0)
        }
    }
    
    // Title and subtitle for use by selection UI.
    public var title: String? { get {
        return self.name
        }
    }
    
    public var subtitle: String? { get {
        return self.category
        }}
}
//lldb command "command script import <filename>"
extension Place {
    func debugQuickLookObject() -> AnyObject?  {
        if let latitude = self.latitude, let longitude = self.longitude {
            return CLLocation(latitude: latitude.doubleValue, longitude: longitude.doubleValue)
        }
        return CLLocation(latitude: 0.0, longitude: 0.0)
    }

}
