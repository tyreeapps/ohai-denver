//
//  DetailViewController.swift
//  ohaidenver
//
//  Created by Walter Tyree.
//  Copyright © 2016 Tyree Apps, LLC. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet var webView : UIWebView!
    var place: Place?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let place = place {
            webView.loadHTMLString(place.detail!, baseURL: nil)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
