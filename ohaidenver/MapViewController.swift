//
//  MapViewController.swift
//  ohaidenver
//
//  Created by Walter Tyree.
//  Copyright © 2016 Tyree Apps, LLC. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    var dataController: DataController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let delegate = UIApplication.sharedApplication().delegate as? AppDelegate else {return}
        dataController = delegate.dataController
        
        NSNotificationCenter.defaultCenter().addObserverForName("Ingestion Done", object: self, queue: nil) { (notification) in
            self.updateAnnotations()
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateAnnotations()
        
    }
    
    func updateAnnotations() {
        if let oldAnnotations = self.mapView?.annotations {
        self.mapView.removeAnnotations(oldAnnotations)
        }
        
        if let places = self.dataController?.thePlaces {
            self.mapView.addAnnotations(places)
            self.mapView.showAnnotations(places, animated: true)
            }
        
    }
}

extension MapViewController: MKMapViewDelegate {
    
}

