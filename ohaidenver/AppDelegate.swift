//
//  AppDelegate.swift
//  ohaidenver
//
//  Created by Walter Tyree.
//  Copyright © 2016 Tyree Apps, LLC. All rights reserved.
//



import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var dataController = DataController()
    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool {
        
        /* if you want to download instead the urls are
         https://www.tyreeapps.com/360idev/points_of_interest.xml
         https://www.tyreeapps.com/360idev/historic_landmarks.xml
         https://www.tyreeapps.com/360idev/park_restrooms.xml
         */
        
        
        let historical_places = NSBundle.mainBundle().URLForResource("historic_landmarks", withExtension: "xml")
        //let downloadOperation = DownloadOperation(source: NSURL(string:"https://www.tyreeapps.com/360idev/park_restrooms")!, destination:nil)
        
        let historicalingestOperation = IngestOperation()
        historicalingestOperation.localURL = historical_places
        //historicalingestOperation.addDependency(downloadOperation)
        
        let queue = NSOperationQueue()
        //queue.addOperation(downloadOperation)
        queue.addOperation(historicalingestOperation)
        
        return true
    }
    
}

