//
//  MapViewController.swift
//  ohaidenver
//
//  Created by Walter Tyree on 8/16/16.
//  Copyright © 2016 Tyree Apps, LLC. All rights reserved.
//

import UIKit
import CoreData
import MapKit

class DenverMapViewController: UIViewController, MKMapViewDelegate {

    var placeLocationController = NSFetchedResultsController()
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let delegate = UIApplication.sharedApplication().delegate as? AppDelegate else {return}
        
        placeLocationController = delegate.dataController.fetchedPlaces()
        
        do {
            try placeLocationController.performFetch()
        
        } catch {
            print(error)
        }
        
        

    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        mapView.mapType = .Standard
        
        if let places = placeLocationController.fetchedObjects as? [Place] {
            for item in places {
                mapView.addAnnotation(item)
            }
        }
    }
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        print(view)
    }
    
    
    func mapView(mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var pin = mapView.dequeueReusableAnnotationViewWithIdentifier("Place")
        
        if pin == nil {
        
        pin = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "Place")
           

        }
        
        pin!.canShowCallout = true
        var calloutButton = UIButton(type:.DetailDisclosure)
        calloutButton.frame = CGRect(x: 0,y: 0,width: 50,height: 50)
        
        pin!.rightCalloutAccessoryView = calloutButton
        
        return pin!
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        print(view)
    }
    

    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        print("Region: \(mapView.region)")
        print("Center Coordinate: \(mapView.centerCoordinate)")
    }
    
}

