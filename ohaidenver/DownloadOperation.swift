//
//  IngestOperation.swift
//  ohaidenver
//
//  Created by Walter Tyree.
//  Copyright © 2016 Tyree Apps, LLC. All rights reserved.
//

//  Borrowed respectfully from
//  Created by Marcus Zarra on 5/5/16.
//  Copyright © 2016 Marcus Zarra. All rights reserved.
//
// I mean, seriously, this guy wrote the book on this stuff.
// https://pragprog.com/book/mzswift/core-data-in-swift
// http://martiancraft.com/blog/2015/03/core-data-stack/
// https://realm.io/news/marcus-zarra-core-data-threading/
//
//

import Foundation

class DownloadOperation: NSOperation, NSURLSessionDownloadDelegate {
    var sourceURL: NSURL?
    var destinationURL : NSURL?
    
    var data = NSData()
    var startTime: NSTimeInterval? = nil
    var totalTime: NSTimeInterval? = nil
    

    
    convenience init(source sURL:NSURL, destination dURL:NSURL?) {
        
        self.init()
        
        self.sourceURL = sURL
        
        if dURL == nil {
        let cachesFolder = try! NSFileManager.defaultManager().URLForDirectory(.CachesDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
        self.destinationURL = cachesFolder.URLByAppendingPathComponent("some_random_file")
        } else {
            self.destinationURL = dURL
        }
    }
    
    var finishedValue: Bool = false
    override var finished: Bool {
        set {
            willChangeValueForKey("isFinished")
            finishedValue = newValue
            didChangeValueForKey("isFinished")
        }
        get {
            return finishedValue
        }
    }
    
    override func start() {
        if cancelled {
            finished = true
            return
        }
        
        guard let url = self.sourceURL else {
            finished = true
            return
        }
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config, delegate: self, delegateQueue: nil)
        
        let request = NSMutableURLRequest(URL: url)
        
        let task = session.downloadTaskWithRequest(request)
        startTime = NSDate.timeIntervalSinceReferenceDate()
        task.resume()
    }
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didFinishDownloadingToURL location: NSURL) {
        
        guard let destinationURL = self.destinationURL else {
            finished = true
            return
        }
        
            do {
                try NSFileManager.defaultManager().removeItemAtURL(destinationURL)
            }
            catch { } //We don't care about this error 
            
            do {
                try NSFileManager.defaultManager().moveItemAtURL(location, toURL: destinationURL)
                totalTime = NSDate.timeIntervalSinceReferenceDate() - startTime!
                finished = true
            }
            catch let error as NSError {
                print("Failed! \(error)")
                finished = true
                return
            }
            
        
    }
    

    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        print("Download progress \(totalBytesWritten) of \(totalBytesExpectedToWrite)")
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        if let error = error {
            print("Failed! \(error)")
            finished = true
            return
        }
        

        totalTime = NSDate.timeIntervalSinceReferenceDate() - startTime!
        finished = true
    }
    
    
    
}





