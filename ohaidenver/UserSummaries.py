import lldb

def place_summary(valueObject, dictionary):
    return valueObject.GetChildMemberWithName('name').GetSummary()
    
def __lldb_init_module(debugger, dict):
    debugger.HandleCommand('type summary add -x .*Place$ -F UserSummaries.place_summary')

