//
//  SecondViewController.swift
//  ohaidenver
//
//  Created by Walter Tyree.
//  Copyright © 2016 Tyree Apps, LLC. All rights reserved.
//

import UIKit
import CoreData

class ListPlacesViewController: UITableViewController {
    
    var dataController: DataController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let delegate = UIApplication.sharedApplication().delegate as? AppDelegate else {return}
        dataController = delegate.dataController
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateData), name: "Ingestion Done", object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func updateData() {
        self.tableView.reloadData()
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  dataController?.thePlaces.count ?? 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellToReturn = tableView.dequeueReusableCellWithIdentifier("Place")
        
        if let cellToReturn = cellToReturn {
            if let place = dataController?.thePlaces[indexPath.row] {
                cellToReturn.textLabel?.text = place.name
            }
            return cellToReturn
        }
        
        return UITableViewCell()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let detail = segue.destinationViewController as? DetailViewController {
            let idx = self.tableView.indexPathForSelectedRow!.row
            let place = dataController?.thePlaces[idx]
            detail.place = place
            
        }
    }
}

